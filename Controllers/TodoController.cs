using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TodoApi.Services;

namespace TodoApi.Controllers
{
    [ApiController]
    [Route("[Controller]")]
    public class TodoController : ControllerBase
    {
        private readonly TodoService _todoService;
        public TodoController(TodoService todoService){
            _todoService = todoService;
        }

        [HttpGet]
        public ActionResult<List<TodoItem>> GetTodoItems()
        {
            return _todoService.Get();
        }

        [HttpGet("{id:length(24)}", Name = "GetTodo")]
        public ActionResult<TodoItem> Get(string id){
            return _todoService.Get(id);
        }

        [HttpPost]
        public ActionResult<TodoItem> Create(TodoItem todo)
        {
            _todoService.Create(todo);

            return CreatedAtRoute("GetTodo", new { id = todo.Id.ToString() }, todo);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, TodoItem bookIn)
        {
            var todoItem = _todoService.Get(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            _todoService.Update(id, bookIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var todoItem = _todoService.Get(id);

            if (todoItem == null)
            {
                return NotFound();
            }

            _todoService.Remove(todoItem.Id);

            return NoContent();
        }
    }

}