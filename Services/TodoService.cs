using MongoDB.Driver;
using TodoApi.Models;
using System.Collections.Generic;

namespace TodoApi.Services
{
    public class TodoService
    {
        private readonly IMongoCollection<TodoItem> _todoItems;
        public TodoService(ITodoDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            _todoItems = database.GetCollection<TodoItem>(settings.TodoCollectionName);
        }

        public List<TodoItem> Get() =>
            _todoItems.Find(todo => true).ToList();

        public TodoItem Get(string id) =>
            _todoItems.Find<TodoItem>(todo => todo.Id == id).FirstOrDefault();

        public TodoItem Create(TodoItem todo)
        {
            _todoItems.InsertOne(todo);
            return todo;
        }

        public void Update(string id, TodoItem bookIn) =>
            _todoItems.ReplaceOne(book => book.Id == id, bookIn);

        public void Remove(TodoItem bookIn) =>
            _todoItems.DeleteOne(book => book.Id == bookIn.Id);

        public void Remove(string id) => 
            _todoItems.DeleteOne(book => book.Id == id);
    }
}